from django.shortcuts import render
import coinwork

btc_address = coinwork.generate_btc_address()

def wallet_home(request):
	content = {
		"btc_public": btc_address[1],
		"btc_private": btc_address[0],
		"btc_balance": coinwork.convert_satoshi_to_btc(int(coinwork.get_btc_balance(btc_address[1])))
	}
	return render(request, 'wallet/index.html', content)